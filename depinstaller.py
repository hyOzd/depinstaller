#!/usr/bin/python3

import argparse
import os
import shutil
import getpass
import subprocess

def init_argparser():
    """Initializes and returns command line argument parser."""
    parser = argparse.ArgumentParser(
        description="Installs given Debian packages as a group ")
    parser.add_argument('group', metavar='G',
                        help="name of the group")
    parser.add_argument('packages', metavar='P', nargs='+',
                        help="package to be installed")

    return parser

def run():
    """Main application"""
    parser = init_argparser()
    args = parser.parse_args()

    # group's Debian package name
    package_name = args.group + "-deps"
    package_version = '1.0'

    # create package directory
    try:
        os.mkdir('package')
    except FileExistsError:
        pass

    # change working directory
    os.chdir('./package')

    # create package root
    try:
        os.mkdir(package_name)
    except FileExistsError:
        print("Cleaning package directory: " + package_name)
        shutil.rmtree('./' + package_name)
        os.mkdir(package_name)

    # create control file
    os.mkdir('./' + package_name + '/DEBIAN')
    with open('./' + package_name + '/DEBIAN/control', 'w') as f:
        f.write("Package: " + package_name + "\n" +
                "Version: " + package_version + "\n" +
                "Architecture: all\n" +
                "Maintainer: " + getpass.getuser() + "\n" +
                "Description: dependencies for " + args.group + " package group\n" +
                "Depends: " + ', '.join(args.packages) + '\n')

    # create Debian package
    subprocess.check_call(['dpkg', '-b', package_name, package_name + '.deb'])

    # install the package with gdebi
    if subprocess.call(['which', 'gdebi']) != 0:
        print("`gdebi` is not installed, skipping installation.")
    else:
        subprocess.call(['sudo', 'gdebi', package_name + '.deb'])

if __name__ == "__main__":
    run()
