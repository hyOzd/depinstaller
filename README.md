# depinstaller.py

This is a small script to install Debian packages as a group. It will
create a dummy package which it's dependencies are the packages that
you want to install. When this dummy package is installed with
`gdebi`, all packages will be marked as "automatically-installed". So
when you want to clean your system, you will remove the dummy/group
package and group packages will be listed as "automatically removable"
which can be removed in a single stroke.

## dependencies

 - python3
 - gdebi-core

## Usage

Make script executable:

`chmod +x ./depinstaller.py`

Run with group name and name of the packages you want to install:

`./depinstaller.py groupname package1 package2 package3`

Dummy group package will be installed as `groupname-deps`. You can
find this package under the auto-created folder "packages".

Also remember `depinstaller.py` requires `gdebi` executable to install
packages. If it doesn't exist you can install with `sudo apt-get
install gdebi-core`.
